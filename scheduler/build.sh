#!/bin/bash
g++ -c microDebug.c -Icomm
g++ -c microObject.c -Icomm
g++ -c mikaApps.cpp -Icomm
g++ -c mikaObject.c -Icomm
g++ -c mikaUID.c -Icomm
g++ -c comm/CScheduler.cpp -Icomm -I../
g++ -c CObject.cpp -Icomm
g++ -c CRunner.cpp -Icomm
g++ -c main.c -Icomm

g++ -o scheduler comm/CSocket.o \
	comm/CSchedulerDelegate.o \
	microDebug.o \
	microObject.o \
	mikaApps.o \
	mikaObject.o \
	mikaUID.o \
	CScheduler.o \
	CObject.o \
	CRunner.o \
	main.o
