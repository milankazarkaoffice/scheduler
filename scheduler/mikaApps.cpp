/**  
  Copyright (C) 2012 Milan Kazarka
  milan.kazarka.office@gmail.com
      
  You may distribute under the terms of the Artistic License, 
  as specified in the README file. You can find a copy of the
  license at LICENSE.
*/

/** 
 *  this is a basic manager of a set of applications
 *  
 */
#include "mikaApps.h"
#include "microDebug.h"
#include "config.h"

#include <signal.h>

static mikaApps *globalApps = NULL;

mikaAppsDelegate::mikaAppsDelegate( )
{
  mprintf("mikaAppsDelegate::mikaAppsDelegate\n");
}

mikaAppsDelegate::~mikaAppsDelegate( )
{
}

mikaAppMsg::mikaAppMsg( unsigned char *buffer )
{
  mprintf("mikaAppMsg::mikaAppMsg\n");
  next = NULL;
  myMsg[0] = 0x00;
  if (buffer)
  {
    strcpy((char*)myMsg,(char*)buffer);
  }
}

mikaAppMsg::~mikaAppMsg( )
{
}

mikaApp::mikaApp( unsigned char *appRoot )
{
  mprintf("mikaApp::mikaApp\n");
  if (!appRoot)
  {
    mprintf("mikaApp::mikaApp warning application root not defined\n");
  }
  else
  {
    delegate = NULL;
    strcpy((char*)myAppRoot,(char*)appRoot);
    strcpy((char*)myAppName," ");
    struct dirent *dp;
    DIR *dir = opendir((char*)appRoot);
    if (dir)
    {
      while((dp = readdir(dir)) != NULL)
      {
        if (dp->d_name[0]=='.')
          continue;
        mprintf("mikaApp::mikaApp root entry(%s)\n",
          (char*)dp->d_name);
        // testing purposes only
        strcpy((char*)myAppName,(char*)dp->d_name);
      }
    }
    else
    {
      mprintf("mikaApp::mikaApp warning directory(%s) error\n",
        (char*)appRoot);
    }
  }
  uid = new mikaUID();
  pID = 0;
  messages = NULL;
  events = NULL;
  addMessageData((unsigned char*)"hello world");
}

mikaApp::mikaApp( unsigned char *command, int root )
{
  mprintf("mikaApp::mikaApp %s\n",(char*)command);
  delegate = NULL;
  myAppRoot[0] = 0x00;
  strcpy((char*)myAppName,(char*)command);
  uid = new mikaUID();
  pID = 0;
  messages = NULL;
  events = NULL;
  started = time(NULL);
  //addMessageData((unsigned char*)"hello world"); // testing
}

mikaApp::~mikaApp( )
{
    printf("mikaApp::~mikaApp\n");
    delete uid;
}

int mikaApp::addMessage( mikaAppMsg *msg )
{
  if (!msg)
    return 1;
  msg->next = messages;
  messages = msg;
  return 0;
}

int mikaApp::addMessageData( unsigned char *msgbuffer )
{
  if (!msgbuffer)
    return 1;
  mikaAppMsg *msg = new mikaAppMsg(msgbuffer);
  if (msg)
    addMessage(msg);
  return 0;
}

/** always run in a synchronized section
 *
 */
int mikaApp::getMessage( unsigned char *msgbuffer )
{
  mprintf("mikaApp::getMessage\n");
  if (!msgbuffer)
  {
    return 1;
  }
  
  mikaAppMsg *current = messages;
  mikaAppMsg *last = NULL;
  while(current)
  {
    if (current->next==NULL)
    {
      strcpy((char*)msgbuffer,(char*)current->myMsg);
      delete current;
      if (last==NULL)
      {
        messages = NULL;
      }
      else
      {
        last->next = NULL;
      }
      return 0;
    }
    last = current;
    current = current->next;
  }
  
  return 1;
}

int mikaApp::getEvent( unsigned char *eventbuffer )
{
  mprintf("mikaApp::getEvent\n");
  if (!eventbuffer)
  {
    mprintf("mikaApp::getEvent error, invalid event buffer\n");
    return 1;
  }
  mikaAppMsg *current = events;
  mikaAppMsg *last = NULL;
  while(current)
  {
    if (current->next==NULL)
    {
      strcpy((char*)eventbuffer,(char*)current->myMsg);
      delete current;
      if (last==NULL)
      {
        events = NULL;
      }
      else
      {
        last->next = NULL;
      }
      return 0;
    }
    last = current;
    current = current->next;
  }
  return 1;
}

int mikaApp::addEvent( mikaAppMsg *event )
{
  if (!event)
  {
    mprintf("mikaApp::addEvent error, invalid event\n");
    return 1;
  }
  event->next = events;
  events = event;
  return 0;
}

int mikaApp::addEventData( unsigned char *eventbuffer )
{
  if (!eventbuffer)
  {
    return 1;
  }
  mikaAppMsg *msg = new mikaAppMsg(eventbuffer);
  if (msg)
    addEvent(msg);
  return 0;
}

int mikaApp::onFinished( )
{
  mprintf("mikaApp::onFinished\n");
  if (delegate)
  {
    if (delegate->onFinish)
    {
      delegate->onFinish(this);
    }
  }
  return 0;
}

int mikaApp::getUUID( unsigned char *buffer )
{
  if (!buffer)
    return 1;
  
  if (uid)
  {
    if (uid->uuid)
    {
      strcpy((char*)buffer,(char*)uid->uuid);
    }
  }
  
  return 0;
}

/** register all applications available in the appDirectory
 */
mikaApps::mikaApps( unsigned char *appDirectory )
{
  mprintf("mikaApps::mikaApps\n");
  if (!appDirectory)
  {
    mprintf("mikaApps::mikaApps warning, no apps root defined, using standard\n");
    appDirectory = (unsigned char*)_MIKA_APP_APPS_ROOT;
  }
  if (globalApps)
  {
    mprintf("mikaApps::mikaApps warning instance of apps already running\n");
  }
  globalApps = this;
  
  pthread_mutex_init(&mutex,NULL);
  sprintf((char*)myAppDirectory,"%s",appDirectory);
  apps = NULL;
  struct dirent *dp;
  unsigned char appRoot[_MIKA_APP_NAMELEN*2];
  mikaApp *app = NULL;
  DIR *dir = opendir((char*)myAppDirectory);
  if (dir)
  {
    while((dp = readdir(dir)) != NULL)
    {
      if (dp->d_name[0]=='.')
        continue;
      sprintf((char*)appRoot,"%s/%s",(char*)myAppDirectory,(char*)dp->d_name);
      mprintf("mikaApps::mikaApps app root(%s)\n",
        (char*)appRoot);
      app = new mikaApp((unsigned char*)appRoot);
      app->next = apps;
      apps = app;
    }
    closedir(dir);
  }
  parentPID = getpid();
}

/** create mikaApps with manualy ading applications 
 *  later
 */
mikaApps::mikaApps( )
{
  globalApps = this;
  
  pthread_mutex_init(&mutex,NULL);
  myAppDirectory[0] = 0x00;
  apps = NULL;
  parentPID = getpid();
}

mikaApps::~mikaApps( )
{
  mprintf("mikaApps::~mikaApps\n");
}

int mikaApps::count( int sync )
{
    int count = 0;
    if (sync==1) pthread_mutex_lock(&mutex);
    mikaApp *current = apps;
    while(current)
    {
        count++;
        current = current->next;
    }
    if (sync==1) pthread_mutex_unlock(&mutex);
    return count;
}

int mikaApps::addApp( mikaApp *app, int sync )
{
  mprintf("mikaApps::addApp\n");
  if (sync==1) pthread_mutex_lock(&mutex);

  if (count(0)>=_MIKA_APPS_MAXPOOL)
  {
      if (sync==1) pthread_mutex_unlock(&mutex);
      return 1;
  }
      
  app->next = apps;
  apps = app;
  if (sync==1) pthread_mutex_unlock(&mutex);
  return 0;
}

/** run the managed application
 */
void *runChild( mikaApp *app, mikaApps *apps )
{
  #ifdef _DEBUG
    if (app->myAppRoot[0]!=0x00)
      printf("runChild path(%s)\n",
        (char*)app->myAppRoot);
  #endif
  unsigned char output[4096];
  unsigned char *command = NULL;
  /** run an app from a defined directory:
   */
  if (app->myAppRoot[0] != 0x00)
  {
    command = (unsigned char*)malloc(
      strlen((char*)app->myAppRoot)+
      strlen((char*)app->myAppName)+2
      );
  
    sprintf((char*)command,"%s/%s",
      (char*)app->myAppRoot,
      (char*)app->myAppName);
  }
  /** make system search for the app
   */
  else
  {
    command = (unsigned char*)malloc(
      strlen((char*)app->myAppName)+2
      );
  
    sprintf((char*)command,"%s",
      (char*)app->myAppName);
  }
  mprintf("runChild command(%s)\n",
    (char*)command);
  if (1)
  {
    FILE *inapp = popen((char*)command,"r");
    while(fgets((char*)output,4096,inapp))
    {
      printf("%s",(char*)output);
    }
    pclose(inapp);
  }
  mprintf("runChild finished %d\n",getpid());
    
    return NULL;
}

void _terminationHandlerEx( int signal )
{
  mprintf("_terminationHandlerEx signal(%d) mikaApps pid(%d) parent(%d)\n",
    signal,getpid(),globalApps->parentPID);
  if (signal!=17)
    exit(0);
}

/** fork the child process of the application app
 *
 */
void *runManaged( mikaApp *app, mikaApps *apps )
{ 
  mprintf("runManaged app(%s)\n",
      (char*)app->uid->uuid);
  int ppid = getpid();
  int pID = fork();
  if (pID == 0)
  {
    signal(SIGHUP,_terminationHandlerEx);
    signal(SIGINT,_terminationHandlerEx);
    signal(SIGTERM,_terminationHandlerEx);
    signal(SIGCHLD,_terminationHandlerEx);
    signal(SIGKILL,_terminationHandlerEx);
    apps->parentPID = ppid;
    globalApps = apps;
    runChild(app,apps);
    exit(EXIT_SUCCESS);
  }
  else if (pID < 0)            // failed to fork
  {
    exit(1); // the whole app must be restarted
  }
  else                        // parent
  {
    mprintf("runManaged managing pid(%d)\n",pID);
    app->pID = pID;
  }
  return NULL;
}

/** run all applications
 */
int mikaApps::runAll( )
{
  mikaApp *current = apps;
  while(current)
  {
    runManaged(current,this);
    current = current->next;
  }
  return 0;
}

int mikaApps::runApp( mikaApp *app)
{
  mprintf("mikaApps::runApp uuid(%s)\n",
    app->uid->uuid);
  runManaged(app,this);
  return 0;
}

/** remove an app from the list of processed apps
 */
int mikaApps::removeApp( mikaApp *app)
{
  if (!app)
    return 1;
  
  mprintf("mikaApps::removeApp\n");
  
  mikaApp *current = apps;
  mikaApp *last = NULL;
  mikaApp *next = NULL;
  while(current)
  {
    next = current->next;
    if (current==app)
    {
      if (last)
        last->next = next;
      else
        apps = next;
      mprintf("mikaApps::removeApp uuid(%s)\n",
        (char*)current->uid->uuid);
      delete current;
      current = NULL;
      return 0;
    }
    last = current;
    current = next;
  }
  
  return 0;
}

/**
 
 #if __DARWIN_UNIX03
 #define WEXITSTATUS(x)	((_W_INT(x) >> 8) & 0x000000ff)
 #else
#define WEXITSTATUS(x)	(_W_INT(x) >> 8)
#endif 
#define WSTOPSIG(x)	(_W_INT(x) >> 8)
#define WIFCONTINUED(x) (_WSTATUS(x) == _WSTOPPED && WSTOPSIG(x) == 0x13)
#define WIFSTOPPED(x)	(_WSTATUS(x) == _WSTOPPED && WSTOPSIG(x) != 0x13)
#define WIFEXITED(x)	(_WSTATUS(x) == 0)
#define WIFSIGNALED(x)	(_WSTATUS(x) != _WSTOPPED && _WSTATUS(x) != 0)
#define WTERMSIG(x)	(_WSTATUS(x))
#if (!defined(_POSIX_C_SOURCE) || defined(_DARWIN_C_SOURCE))
#define WCOREDUMP(x)	(_W_INT(x) & WCOREFLAG)

#define	W_EXITCODE(ret, sig)	((ret) << 8 | (sig))
#define	W_STOPCODE(sig)		((sig) << 8 | _WSTOPPED)
#endif
 
 */
void *mikaAppsProcessingEx(void*data)
{
    if (!data)
        return NULL;
    
    mprintf("mikaAppsProcessing\n");
    mikaApps *apps = (mikaApps*)data;
    mikaApp *current;
    int status;
    
    while(1)
    {
        pid_t pid = waitpid(-1, &status, 0);
        if (pid != -1)
        {
            printf("STATUS %d pid %d\n",status,pid);
            /**
            if (
                WIFEXITED(status) ||
                WIFSIGNALED(status) ||
                WIFSTOPPED(status) ||
                WIFCONTINUED(status) ||
                //
                WTERMSIG(status) ||
                WCOREDUMP(status)
                )
            {
            */
                pthread_mutex_lock(&apps->mutex);
                current = apps->apps;
                while(current)
                {
                    if (current->pID==pid)
                    {
                        printf("mikaAppsProcessing process(%d) exited, status=%d mypid(%d)\n", current->pID, WEXITSTATUS(status),getpid());
                        current->onFinished();
                        apps->removeApp(current);
                        break;
                    }
                    current = current->next;
                }
                if (current==NULL)
                {
                    printf("ERROR 01 pid %d\n",pid);
                }
                pthread_mutex_unlock(&apps->mutex);
            //}
            //else
            //{
            //    //printf("ERROR 02\n");
            //}
        } // pid present
    }
    
    return NULL;
}

/** application processing thread
 
 It seems that the way the signals are processed is unproductive and we're running one too many loops - this implementation has prooven to function.
 
 This proves to do a good job when cleaning up jobs. It receives the needed signals and handles them.
 
 */
void *mikaAppsProcessing(void*data)
{
  if (!data)
    return NULL;
    
  mprintf("mikaAppsProcessing\n");
  mikaApps *apps = (mikaApps*)data;
  mikaApp *current;
  int status;
  int n = 0;
  while(1)
  {
    pthread_mutex_lock(&apps->mutex); // to access the list of apps you need to lock the mikaApps while accessing
    current = apps->apps;
    while(current)
    {
      if (current->pID==0)
      {
        usleep(_MIKA_APPS_CLOCKTIK);
        current = current->next;
        continue;
      }
      if (0) printf("mikaAppsProcessing getpid(%d)\n",getpid());
      if ( waitpid(current->pID, &status, WNOHANG) != 0)
      {
        if (WIFEXITED(status)) {
          printf("mikaAppsProcessing process(%d) exited, status=%d mypid(%d)\n", current->pID, WEXITSTATUS(status),getpid());
          current->onFinished();
          apps->removeApp(current);
          break;
        } else if (WIFSIGNALED(status)) {
          printf("mikaAppsProcessing process(%d) killed by signal %d mypid(%d)\n", current->pID, WTERMSIG(status),getpid());
          current->onFinished();
          apps->removeApp(current);
          break;
        } else if (WIFSTOPPED(status)) {
          printf("mikaAppsProcessing process(%d) stopped by signal %d mypid(%d)\n", current->pID, WSTOPSIG(status),getpid());
          current->onFinished();
          apps->removeApp(current);
          break;
        } else if (WIFCONTINUED(status)) {
          printf("mikaAppsProcessing process(%d) continued mypid(%d)\n",current->pID,getpid());
        }
      }
      current = current->next;
      n++;
    }
    pthread_mutex_unlock(&apps->mutex);
    usleep(_MIKA_APPS_CLOCKTIK);
  }
}

/** process the states of child processes
 */
int mikaApps::runProcessing( )
{
  pthread_t t;
  pthread_create(&t,NULL,mikaAppsProcessing,this);
  return 0;
}

int mikaApps::stopAll( )
{
  mprintf("mikaApps::stopAll\n");
  pthread_mutex_lock(&mutex);
  mikaApp *current = apps;
  while(current)
  {
    mprintf("mikaApps::kill pid(%d)\n",current->pID);
    kill(current->pID,2);
    current = current->next;
  }
  pthread_mutex_unlock(&mutex);
  return 0;
}

/** this is a function to kill child processes of a parent
 *  process in case we don't know their PID. This is Linux
 *  speciffic, since it uses an external command's output.
 */
int mikaApps::stopChildrenNamed( unsigned char *name )
{
  mprintf("mikaApps::stopChildrenNamed name(%s) pid(%d)\n",
    (char*)name,getpid());
  FILE *po = NULL;
  unsigned char output[1024];
  unsigned char complete[4096];
  unsigned char command[1024];
  
  sprintf((char*)command,"ps -ejH | grep %d",getpid());
  
  po = popen((char*)command,"r");
  if (po)
  {
    strcpy((char*)complete,"");
    while(fgets((char*)output,4096,po))
    {
      mprintf("\tout: %s\n",(char*)output);
      strcat((char*)complete,(char*)output);
      if (strstr((char*)output,(char*)name))
      {
        mprintf("\thit(%s)\n",(char*)output);
        int n;
        int end = 0;
        int start = -1;
        for(n = 0; n < strlen((char*)output); n++)
        {
          if (output[n]!=' ')
          {
            if (start==-1)
              start = n;
          }
          else
          {
            if (start!=-1)
            {
              end = n;
              break;
            }
          }
        }
        if (start!=-1 && end!=0)
        {
          unsigned char schild[16];
          memcpy((char*)schild,output+start,end-start);
          schild[end-start] = 0x00;
          printf("\tkill(%s) start(%d) end(%d)\n",
            (char*)schild,start,end);
          kill(atoi((char*)schild),SIGTERM);
        }
      }
    }
    pclose(po);
  }
  
  return 0;
}

int mikaApps::lock( )
{
  pthread_mutex_lock(&mutex);
  return 0;
}

int mikaApps::unlock( )
{
  pthread_mutex_unlock(&mutex);
  return 0;
}

/** needs to be ran inside a lock() / unlock() section
 *
 */
mikaApp *mikaApps::getApp( unsigned char *uuid )
{
  mikaApp *app = NULL;
  mikaApp *current = apps;
  while(current)
  {
    if (strcmp((char*)current->uid->uuid,(char*)uuid)==0)
    {
      app = current;
    }
    current = current->next;
  }
  return app;
}

int mikaApps::getMessageForUUID( unsigned char *uuid, unsigned char *msgbuffer )
{
  mprintf("mikaApps::getMessageForUUID\n");
  if (!uuid || !msgbuffer)
  {
    mprintf("mikaApps::getMessageForUUID error, invalid arguments\n");
    return 1;
  }
  lock();
  mikaApp *app = getApp(uuid);
  if (app)
  {
    if ( app->getMessage( msgbuffer ) == 0 )
    {
      unlock();
      return 0;
    }
    else
    {
      mprintf("mikaApps::getMessageForUUID warning, no message\n");
    }
  }
  else
  {
    mprintf("mikApps::getMessageForUUID warning, no app of uuid(%s)\n",
      (char*)uuid);
  }
  unlock();
  return 1;
}

int mikaApps::putMessageForUUID( unsigned char *uuid, unsigned char *msgbuffer )
{
  if (!uuid || !msgbuffer)
  {
    return 1;
  }
  lock();
  mikaApp *app = getApp(uuid);
  if (app)
  {
    if ( app->addMessageData( msgbuffer ) == 0 )
    {
      unlock();
      return 0;
    }
  }
  unlock();
  return 1;
}

int mikaApps::getEventForUUID( unsigned char *uuid, unsigned char *eventbuffer )
{
  mprintf("mikaApps::getEventForUUID\n");
  if (!uuid || !eventbuffer)
  {
    mprintf("mikaApps::getEventForUUID error, invalid arguments\n");
    return 1;
  }
  lock();
  mikaApp *app = getApp(uuid);
  if (app)
  {
    if ( app->getEvent( eventbuffer ) == 0 )
    {
      unlock();
      return 0;
    }
    else
    {
      mprintf("mikaApps::getEventForUUID warning, no event\n");
    }
  }
  else
  {
    mprintf("mikApps::getEventForUUID warning, no app of uuid(%s)\n",
      (char*)uuid);
  }
  unlock();
  return 1;
}

int mikaApps::putEventForUUID( unsigned char *uuid, unsigned char *eventbuffer )
{
  mprintf("mikaApps::putEventForUUID\n");
  if (!uuid || !eventbuffer)
  {
    return 1;
  }
  lock();
  mikaApp *app = getApp(uuid);
  if (app)
  {
    if ( app->addEventData( eventbuffer ) == 0 )
    {
      unlock();
      return 0;
    }
  }
  unlock();
  return 1;
}

int mikaApps::putEvent( unsigned char *eventbuffer )
{
  mprintf("mikaApps::putEvent\n");
  if (!eventbuffer)
  {
    return 1;
  }
  lock();
  mikaApp *current = apps;
  while(current)
  {
    current->addEventData( eventbuffer );
    current = current->next;
  }
  unlock();
  return 0;
}

/** kill an application by it's UUID
 *
 */
int mikaApps::killAppUUID( unsigned char *uuid )
{
  mprintf("mikaApps::killAppUUID\n");
  if (!uuid)
  {
    mprintf("mikaApps::killAppUUID error, invalid uuid\n");
    return 1;
  }
  lock();
  mikaApp *app = getApp(uuid);
  if (!app)
  {
    unlock();
    return 1;
  }
  
  
  FILE *po = NULL;
  unsigned char output[1024];
  unsigned char superpid[32];

  unsigned char command[1024];
  printf("KILL fetch(%d)\n",app->pID);
  
  sprintf((char*)command,"./killtree.sh %d",app->pID);

  superpid[0] = 0x00;
  po = popen((char*)command,"r");
  if (po)
  {
    while(fgets((char*)output,4095,po))
    {      
    }
    pclose(po);
  }
  
  unlock();
  return 0;
}

int mikaApps::killApp( mikaApp *app )
{
  if (!app)
    return 1;
  
  lock();
  
  FILE *po = NULL;
  unsigned char output[1024];
  unsigned char superpid[32];
 
  unsigned char command[1024];
  printf("KILL fetch(%d)\n",app->pID);
  
  sprintf((char*)command,"./killtree.sh %d",app->pID);

  superpid[0] = 0x00;
  po = popen((char*)command,"r");
  if (po)
  {
    while(fgets((char*)output,4095,po))
    {      
    }
    pclose(po);
  }
  
  unlock();
  
  return 0;
}

int mikaApps::isUUID( unsigned char *uuid )
{
  if (!uuid)
  {
    mprintf("mikaApps::isUUID error, invalid uuid\n");
    return 0;
  }
  
  if (getApp(uuid))
    return 1;
    
  return 0;
}

int mikaApps::list( )
{
    printf("mikaApps::list\n");
    lock();
    mikaApp *current = apps;
    while(current)
    {
        printf("mikaApps::list  app %s running %ld\n",(char*)current->uid->uuid,time(NULL)-current->started);
        current = current->next;
    }
    unlock();
    return 0;
}

/**
 get a list of all running apps in an XML list
 */
unsigned char *mikaApps::getXmlAppsList( )
{
    unsigned char *xlist = NULL;
    unsigned char tmp[128];
    
    lock();
    int acount = count(0);
    xlist = (unsigned char*)malloc((acount*128)+128);
    strcpy((char*)xlist,"<apps>\n");
    
    mikaApp *current = apps;
    while(current)
    {
        sprintf((char*)tmp,"<app uuid=\"%s\"></app>\n",(char*)current->uid->uuid);
        strcat((char*)xlist,(char*)tmp);
        current = current->next;
    }
    unlock();
    
    strcat((char*)xlist,"</apps>\n");
    
    return xlist; // needs to be freed by caller
}
