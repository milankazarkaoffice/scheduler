//
//  CLog.cpp
//  scheduler
//
//  Created by Milan Kazarka on 10/10/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#include "CLog.h"
#include "CCommon.h"

CLog *gclog = NULL;

CLog *getCLog( )
{
    if (!gclog)
        gclog = new CLog();
    
    return gclog;
}

CLog::CLog( )
{
    pthread_mutex_init(&mutex,NULL);
}

CLog::~CLog( )
{
    
}

int CLog::add( unsigned char *msg )
{
    if (!msg)
        return 1;
    
    lock();
    
    unlock();
    
    return 0;
}

int CLog::lock( )
{
    pthread_mutex_lock(&mutex);
    return 0;
}

int CLog::unlock( )
{
    pthread_mutex_unlock(&mutex);
    return 0;
}
