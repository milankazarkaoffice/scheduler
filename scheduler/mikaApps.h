/**  
  Copyright (C) 2012 Milan Kazarka
  milan.kazarka.office@gmail.com
      
  You may distribute under the terms of the Artistic License, 
  as specified in the README file. You can find a copy of the
  license at LICENSE.
*/
#ifndef _MIKA_APPS_H_
#define _MIKA_APPS_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <dirent.h>
#include <pthread.h>

#include "mikaUID.h"
#include "mikaObject.h"
#include "microObject.h"

#define _MIKA_APP_APPS_ROOT 	"apps"
#define _MIKA_APP_NAMELEN 	512
#define _MIKA_APPS_CLOCKTIK 	10000
#define _MIKA_APP_MSGLEN 	4096
#define _MIKA_APPS_MAXPOOL 50 // the maximum number of apps being processed simultaneously

/** handling a list of applications, whereas we don't have
 *  the actual mikaApp objects, just some info
 */
typedef struct appRegisterObject {
  unsigned char uuid[64];
  struct appRegisterObject *next;
} appRegisterObject;

typedef struct mikaAppDelegate {
  void *(*onFinish)( void *appData );
  microObject *receiver;
} mikaAppDelegate;

/** IPC messaging the simple way. Without SHM.
 *
 */
class mikaAppMsg {
  public:
    unsigned char myMsg[_MIKA_APP_MSGLEN+1];
    mikaAppMsg *next;
    
    mikaAppMsg( unsigned char *buffer );
    ~mikaAppMsg( );
};

/** defining an application running on a system
 */
class mikaApp : public mikaObject, microObject {
  public:
    mikaApp *next;
    unsigned char myAppRoot[_MIKA_APP_NAMELEN*2];
    unsigned char myAppName[_MIKA_APP_NAMELEN];
    int pID; // only used by parent process
    mikaUID *uid;
    long started;
    struct mikaAppDelegate *delegate;
    
    mikaAppMsg *messages; // IPC messages
    mikaAppMsg *events; // user events (like touch events)
    
    mikaApp( unsigned char *appRoot );
    mikaApp( unsigned char *command, int root );
    ~mikaApp( );
    
    int onFinished();
    int getMessage( unsigned char *msgbuffer );
    int addMessage( mikaAppMsg *msg );
    int addMessageData( unsigned char *msgbuffer );
    
    int getEvent( unsigned char *eventbuffer );
    int addEvent( mikaAppMsg *event );
    int addEventData( unsigned char *eventbuffer );
    
    int getUUID( unsigned char *buffer );
};

typedef struct mikaAppsDelegate_t {
} mikaAppsDelegate_t;

class mikaAppsDelegate : public mikaObject {
  public:
    struct mikaAppsDelegate_t delegate;
    
    mikaAppsDelegate( );
    ~mikaAppsDelegate( );
};

class mikaApps : public mikaObject, microObject {
  public:
    mikaApp *apps;
    unsigned char myAppDirectory[_MIKA_APP_NAMELEN];
    pthread_mutex_t mutex;
    int parentPID;
  
    /** automatically start a set of applications
     *  residing in a speciffic directory
     */
    mikaApps( unsigned char *appDirectory );
    /** just create
     */
    mikaApps( );
    ~mikaApps( );
    
    int count( int sync ); // number of running jobs / apps registered
    int addApp( mikaApp *app, int sync );
    mikaApp *getApp( unsigned char *uuid );
    int getMessageForUUID( unsigned char *uuid, unsigned char *msgbuffer );
    int putMessageForUUID( unsigned char *uuid, unsigned char *msgbuffer );
    int getEventForUUID( unsigned char *uuid, unsigned char *eventbuffer );
    int putEventForUUID( unsigned char *uuid, unsigned char *eventbuffer );
    int putEvent( unsigned char *eventbuffer );
    int runAll( );
    int stopAll( );
    int stopChildrenNamed( unsigned char *name );
    int runApp( mikaApp *app );
    int killAppUUID( unsigned char *uuid );
    int killApp( mikaApp *app );
    int isUUID( unsigned char *uuid ); // 0=no 1=yes
    int runProcessing( );
    int removeApp( mikaApp *app );
    int lock( );
    int unlock( );
    int list( ); // list all running apps
    unsigned char *getXmlAppsList( );
};

#endif
