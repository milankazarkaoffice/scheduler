//
//  CSocket.cpp
//  orderplug
//
//  Created by Milan Kazarka on 3/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#include "CSocket.h"
#include "CCommon.h"
#include "CScheduler.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <pthread.h>

void sigchld_handler(int s)
{
    while(waitpid(-1, NULL, WNOHANG) > 0);
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void *handleConnections(void *data)
{
    CSocket *cs = (CSocket*)data;
    cs->handleConnectionsThread();
    return NULL;
}

CSocket::CSocket( )
{
}

void *handleConnectionThread( void *data )
{
    struct connectionDescription *description = (struct connectionDescription*)data;
    description->cs->handleConnection(description->newfd);
    free(description);
    return NULL;
}

void *handleConnectionTestThread( void *data )
{
    
    return NULL;
}

void CSocket::handleConnection( int newfd )
{
    
    if (sdelegate)
    {
        CScheduler *s = (CScheduler*)sdelegate;
        s->onNewConnection(this,newfd);
    }
    
    close(newfd);
}

void CSocket::handleConnectionsThread( )
{
    int new_fd;
    socklen_t sin_size;
    struct sockaddr_storage their_addr;
    char s[INET6_ADDRSTRLEN];
    
    while(1) {
        sin_size = sizeof their_addr;
        new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
        if (new_fd == -1) {
            perror("accept");
            continue;
        }
         
        inet_ntop(their_addr.ss_family,
                  get_in_addr((struct sockaddr *)&their_addr),
                  s, sizeof s);
#ifdef _DEBUG
        printf("CSocket::handleConnectionThread got connection from %s\n", s);
#endif
        
        struct connectionDescription *description = (struct connectionDescription*)malloc(sizeof(struct connectionDescription));
        description->cs = this;
        description->newfd = new_fd;
        
        pthread_t t;
        int rc = pthread_create(&t, NULL, &handleConnectionThread, (void*)description);
        if (rc == 0) {
            rc = pthread_detach(t);
        }
        /**
        pthread_t t;
        int rc = pthread_create(&t, NULL, &handleConnectionTestThread, NULL);
        if (rc == 0) {
            rc = pthread_detach(t);
        }
        
        close(new_fd);
        */
    }
}

void CSocket::listenOnPort( int port )
{    
    struct addrinfo hints, *servinfo, *p;
    struct sigaction sa;
    int yes=1;
    int rv;
    
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    
    char c[20];
    sprintf(c, "%d", port);
    
    if ((rv = getaddrinfo(NULL, (char*)c, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return;
    }
    
    // loop through all the results and bind to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                             p->ai_protocol)) == -1) {
            perror("server: socket");
            continue;
        }
        
        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
                       sizeof(int)) == -1) {
            perror("setsockopt");
            exit(1);
        }
        
        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("server: bind");
            continue;
        }
        
        break;
    }
    
    if (p == NULL)  {
        fprintf(stderr, "server: failed to bind\n");
        return;
    }
    
    freeaddrinfo(servinfo); // all done with this structure
    
    if (listen(sockfd,10) == -1) {
        perror("listen");
        exit(1);
    }
    
    sa.sa_handler = sigchld_handler; // reap all dead processes
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        perror("sigaction");
        exit(1);
    }
    
    pthread_t t;
    pthread_create(&t, NULL, &handleConnections, this);
    
}

int CSocket::readSocket( int fd, unsigned char *data, int size )
{
    if (!data)
    {
#ifdef _DEBUG
        printf("CSocket::readSocket error, invalid data\n");
#endif
        return 0;
    }
    
    int rc = 0;
    
    rc = (int)recv(fd, data, size, 0);
    if (rc<0)
    {
        printf("CSocket::readSocket error, can't read on socket\n");
        return 0;
    }
    
    return rc;
}

int CSocket::writeSocket( int fd, unsigned char *data, int size )
{
    if (!data)
    {
#ifdef _DEBUG
        printf("CSocket::writeSocket error, invalid data\n");
#endif
        return 1;
    }
    
    int rc = 0;
    rc = (int)send(fd, (char*)data, (int)size, 0);
    if (rc == -1)
        perror("send");
    return rc;
}

void *get_in_addr_ex(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

/**
 perform a handshake with a server
 1. create socket
 2. send data
 3. receive response
 */
int CSocket::sendData( unsigned char *serverip, int port, unsigned char *sending, unsigned char *reply )
{
#ifdef _DEBUG
    printf("CSocket::sendData\n");
#endif
    int rc = 0;
    
    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    char s[INET6_ADDRSTRLEN];
    
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    
    // getaddrinfo(const char *hostname, const char *servname, const struct addrinfo *hints,struct addrinfo **res);

    unsigned char portchars[7];
    sprintf((char*)portchars,"%d",port);
    
    //int err = getaddrinfo((char*)serverip, (char*)portchars, 0, &servinfo);
    int err;
    if ((err = getaddrinfo((char*)serverip, (char*)portchars, &hints, &servinfo)) != 0) {
#ifdef _DEBUG
        printf("client: failed to retrieve server info\n");
#endif
        return 0;
    }
    
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                             p->ai_protocol)) == -1) {
            perror("client: socket");
            continue;
        }
        
        if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("client: connect");
            continue;
        }
        
        break;
    }
    
    if (p == NULL) {
#ifdef _DEBUG
        printf("client: failed to connect\n");
#endif
        return 0;
    }
    
    inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
              s, sizeof s);
#ifdef _DEBUG
    printf("client: connecting to %s\n", s);
#endif
    freeaddrinfo(servinfo); // all done with this structure
    
    // handle the socket
    int ret = writeSocket(sockfd, (unsigned char*)sending, (int)strlen((char*)sending));
#ifdef _DEBUG
    printf("written %d\n",ret);
#endif
    ret = readSocket(sockfd, (unsigned char*)reply, _MSGBUFFER);
#ifdef _DEBUG
    printf("read %d\n",ret);
#endif
    if (ret>0)
    {
        reply[ret] = 0x00;
#ifdef _DEBUG
        printf("read data(%s)\n",(char*)reply);
#endif
        rc = ret; // caller of this method gets the number of bytes in the reply
    }
    //
            
    close(sockfd);
    
    return rc;
}

