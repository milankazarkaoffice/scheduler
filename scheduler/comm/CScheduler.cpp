//
//  CScheduler.cpp
//  scheduler
//
//  Created by Milan Kazarka on 8/3/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#include "CScheduler.h"
#include "CCommon.h"
#include "main.h"

CScheduler::CScheduler( )
{
    printf("CScheduler::CScheduler\n");
}

CScheduler::~CScheduler( )
{
    
}

void CScheduler::onNewConnection( CSocket *cs, int newfd )
{
    printf("CScheduler::onNewConnection\n");
    
    unsigned char out[_MSGBUFFER];
    unsigned char in[_MSGBUFFER];
    out[0] = 0x00;
    
    int size = cs->readSocket(newfd, (unsigned char*)in, _MSGBUFFER-1);
    if (size>0) // 8? - check for buffer overruns
    {
        in[size] = 0x00;
        printf("CScheduler::onNewConnection request %s\n",(char*)in);
        
        // process the request we've received
        
        // RUN:command args - runs the command in the default shell asynchroniously.
        //
        //      Returns the UUID of the process
        
        if (
            strncmp((char*)in,"RUN:",4)==0
            )
        {
            unsigned char *command = (unsigned char*)in+4;
            command[strlen((char*)command)-2] = 0x00; // \n
            
            printf("run command '%s'\n",(char*)command);
            
            unsigned char uuidcopy[64];
            uuidcopy[0] = 0x00;
            mikaApp *testApp = new mikaApp((unsigned char*)command,0);
            if (testApp->uid->uuid)
            {
                strcpy((char*)uuidcopy,(char*)testApp->uid->uuid);
                strcpy((char*)out,(char*)uuidcopy);
            }
            
            if ( apps->addApp(testApp, 1) == 0 )
            {
                apps->runApp(testApp);
                //apps->removeApp(testApp);
            }
            else
            {
                delete testApp;
            }
            
            apps->list();
        }
        
        // log an event that didn't go through the scheduler
        
        else if (
            strncmp((char*)in,"LOG:",4)==0
            )
        {
            
        }
            
        // SYNC: runs a command synchroniously within this process & this thread
        
        else if (
            strncmp((char*)in,"SYNC:",5)==0
            )
        {
            
            unsigned char *command = (unsigned char*)in+5;
            command[strlen((char*)command)-2] = 0x00; // \n
            
            FILE *fp;
            char output[_MSGBUFFER];
            
            // Open the command for reading.
            fp = popen((char*)command, "r");
            if (fp == NULL) {
                printf("Failed to run command\n" );
                return;
            }
            
            // Read the output a line at a time - output it.
            while (fgets(output, sizeof(output)-1, fp) != NULL)
            {
                printf("%s", output);
                strcpy((char*)out,(char*)output);
            }
            
            // close
            pclose(fp);
            
        }
        
        // get the description of running jobs
        
        else if (
                 strncmp((char*)in,"JOBS:",5)==0
                 )
        {
            unsigned char *xlist = apps->getXmlAppsList();
            cs->writeSocket(newfd, (unsigned char*)xlist, (int)strlen((char*)xlist));
            free(xlist);
        }
        
        // do we accept a new job? (0/1)
        
        else if (
            strncmp((char*)in,"CANRUN:",7)==0
            )
        {
            
            if ( apps->count(1) >= _MIKA_APPS_MAXPOOL)
            {
                strcpy((char*)out,(char*)"0");
            }
            else
                strcpy((char*)out,(char*)"1");
        }
        
        if (out[0]!=0x00)
            cs->writeSocket(newfd, (unsigned char*)out, (int)strlen((char*)out));
    }
}
