//
//  CScheduler.h
//  scheduler
//
//  Created by Milan Kazarka on 8/3/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#ifndef __scheduler__CScheduler__
#define __scheduler__CScheduler__

#include <iostream>
#include "CSchedulerDelegate.h"
#include "CSocket.h"

class CScheduler : public CSchedulerDelegate {
public:    
    CScheduler( );
    ~CScheduler( );
    
    void onNewConnection( CSocket *cs, int newfd );
};

#endif /* defined(__scheduler__CScheduler__) */
