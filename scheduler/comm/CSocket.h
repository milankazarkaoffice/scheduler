//
//  CSocket.h
//  orderplug
//
//  Created by Milan Kazarka on 3/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#ifndef __orderplug__CSocket__
#define __orderplug__CSocket__

#include <stdio.h>
#include "CSchedulerDelegate.h"
#include "CObject.h"

class CSocket : public CObject {
public:
    int sockfd;
    CSchedulerDelegate *sdelegate;
    
    CSocket( );
    void listenOnPort( int port );
    void handleConnectionsThread( );
    void handleConnection( int newfd );
    
    int readSocket( int fd, unsigned char *data, int size );
    int writeSocket( int fd, unsigned char *data, int size );
    
    int sendData( unsigned char *serverip, int port, unsigned char *sending, unsigned char *reply );
};

struct connectionDescription {
    CSocket *cs;
    int newfd;
};

#endif /* defined(__orderplug__CSocket__) */
