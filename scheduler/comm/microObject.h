/**  
  Copyright (C) 2012 Milan Kazarka
  milan.kazarka.office@gmail.com
      
  You may distribute under the terms of the Artistic License, 
  as specified in the README file. You can find a copy of the
  license at LICENSE.
*/
#ifndef _MICRO_OBJECT_H_
#define _MICRO_OBJECT_H_

#include "config.h"
#include "common.h"

class microObject {
  public:
    microObject( );
    ~microObject( );
};

#endif
