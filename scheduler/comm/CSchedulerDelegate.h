//
//  CSchedulerDelegate.h
//  scheduler
//
//  Created by Milan Kazarka on 8/3/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#ifndef __scheduler__CSchedulerDelegate__
#define __scheduler__CSchedulerDelegate__

#include <iostream>
#include "CObject.h"
#include <stdio.h>

class CSchedulerDelegate : public CObject {
public:
    virtual void onNewConnection( CObject *cs, int newfd ) { printf("onNewConnection\n"); }
};

#endif /* defined(__scheduler__CSchedulerDelegate__) */
