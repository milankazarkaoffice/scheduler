//
//  main.h
//  scheduler
//
//  Created by Milan Kazarka on 8/3/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#ifndef scheduler_main_h
#define scheduler_main_h

#include "CSocket.h"
#include "CScheduler.h"
#include "mikaApps.h"

extern CScheduler *appScheduler;
extern CSocket *appSocket;
extern mikaApps *apps;

#endif
