//
//  main.c
//  scheduler
//
//  Created by Milan Kazarka on 8/3/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "main.h"
#include "CCommon.h"
#include "CLog.h"

CScheduler *appScheduler;
CSocket *appSocket;
mikaApps *apps;

int main(int argc, const char * argv[])
{
    getCLog(); // initialise logging
    
    appScheduler = new CScheduler();
    appSocket = new CSocket();
    appSocket->sdelegate = appScheduler;
    appSocket->listenOnPort(_PORT);
    
    apps = new mikaApps();
    apps->runProcessing();
    
    //mikaApp *testApp = new mikaApp((unsigned char*)"/Applications/Gimp.app/Contents/MacOS/gimp-2.8",0);
    /**
    mikaApp *testApp = new mikaApp((unsigned char*)"top",0);
    
    if ( apps->addApp(testApp, 1) == 0 )
    {
        //apps->runApp(testApp);
    }
    apps->list();
    */
    // you can work on an app and retrieve it by:
    //  1. locking apps
    //  2. retrieving the app object
    //  3. working with it based on the UDID
    
    
    while(1) { sleep(1); }
    
    return 0;
}

