//
//  CLog.h
//  scheduler
//
//  Created by Milan Kazarka on 10/10/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#ifndef __scheduler__CLog__
#define __scheduler__CLog__

#include <iostream>
#include "CObject.h"
#include "pthread.h"

class CLog : public CObject {
    pthread_mutex_t mutex;
public:
    CLog( );
    ~CLog( );
    
    int add( unsigned char *msg );
    int lock();
    int unlock();
};

extern CLog *gclog;
CLog *getCLog( );

#endif /* defined(__scheduler__CLog__) */
