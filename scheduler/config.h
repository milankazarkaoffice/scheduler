/*
 *    Copyright (C) 2010,2011,2012 by Milan Kazarka
 *
 *    You may distribute under the terms of the Artistic License,
 *    as specified in the README file.
 *
 */

#ifndef _CONFIG_H_
#define _CONFIG_H_

#define _CF_PLATFORM_UNIX
#define _CF_GUI_XLIB

#define USECAIRO

#ifdef USECAIRO
  #define _CAIRO_DEVICE_NO
#endif

#define USERSS_NO
#define _RSSTYPE_MRSS

#define		TRUE		1
#define		FALSE		0
typedef unsigned short int	BOOL;

#ifndef _DEBUG
  #define _DEBUG
  #define _POLL_DEBUG
#endif

#define _LARGE			4096
#define _FAST_BUFFERS

#define _STRESS_NO

#endif
